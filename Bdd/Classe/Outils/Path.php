<?php



class Path{

    public $fichier = [];

    public function __construct() {
        $this->mkmap(ROOT);
    }
    private function mkmap($dir) {
        // echo "<ul>";
        $folder = opendir ($dir);
        while ($file = readdir ($folder)) {
            if ($file != "." && $file != "..") {
                $pathfile = $dir.'/'.$file;
                if (ctype_upper($file[0]) && strstr($file, 'php')){
                    if(filetype($pathfile) != 'dir'){
                        array_push($this->fichier, $pathfile) ;
                    }
                }
                // echo "<li><a href=$pathfile>$file</a></li>";
                if(filetype($pathfile) == 'dir'){
                    $this->mkmap($pathfile);
                }
            }
        }
    }

}
 ?>
