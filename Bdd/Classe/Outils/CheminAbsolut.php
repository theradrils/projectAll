<?php


	class CheminAbsolut {

		public static function lancement() {
			$realPath = __DIR__;
			$path = explode('/', $realPath);
			$chemin = (array_slice($path, 0, count($path)-2));
			$chemin = implode('/', $chemin);
			return $chemin;
		}
	}

 ?>
