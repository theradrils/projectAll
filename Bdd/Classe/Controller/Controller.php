<?php
  class Controller
  {
      protected $navTable;
      protected $mod_Admin;
      protected $page;
      protected $vue;

      public function __construct($page)
      {
          $this->page        = $page; //page demandé
          $this->mod_Admin   = new Admin(); //instanie un Modele admin
          $this->navTable    = $this->mod_Admin->getAllTable();//Reuperere le nom des tables pour en faire un menu
      }
  }//end Class
