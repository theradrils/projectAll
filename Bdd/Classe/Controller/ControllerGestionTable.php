<?php

  class ControllerGestionTable extends Controller {

    private $table;

    public function __construct($page)  {

        parent::__construct($page);
        $this->setTable();
    }//end construt

    public function pageGestionTable()  {

        if (!empty($_POST)) {
          $this->traitementPost();
        }//end if

        $this->setTable();

        $reponse = $this->mod_Admin->getTable($this->table);
        $this->vue  = new FabriqueVueAdmin($this->page);
        $this->vue->fabriquePage($this->navTable, array('table' => $reponse));

    }//end Function

    private function setTable() {

        if (isset($_GET['table'])) {
            $this->table = $_GET['table'];

        } else {
            $this->table = 'admin';
        }//end if

    }// end function

      private function traitementPost() {

          if (isset($_POST['supprimer'])) {
              $tbl = explode(' ', $_POST['supprimer']);
              $this->mod_Admin->supprLigne($_GET['table'], $tbl);

          }//end if

          if (isset($_POST['valeur'])) {
              $table =  $_GET['table'];
              $tbl = json_decode($_POST['valeur']);
              $this->mod_Admin->modifierTable($table, $tbl);
          }//end if

          if (isset($_POST['ajout'])) {
              $table =  $_GET['table'];
              $tbl = json_decode($_POST['ajout']);
              $this->mod_Admin->ajoutTable($table, $tbl);
          }//end if
          
      }//end function
  }//end Class
