<?php

class Router  {

    private $ctrl_Accueil;
    private $ctrl_GestionBdd;
    private $ctrl_GestionTable;
    private $pageReq;

    public function __construct() {

        if (isset($_GET['action'])) {
            $this->pageReq = $_GET['action'];
        } else {
            $this->pageReq = 'accueil';
        }//end if

    }//end construct

    public function routerReq() {
      
      switch ($this->pageReq) {

        case 'accueil':
            $this->ctrl_Accueil = new ControllerAccueil('accueil');
            $this->ctrl_Accueil->pageAccueil();
            break;

        case 'gestiontable':
          $this->ctrl_GestionTable = new ControllerGestionTable('gestiontable');
          $this->ctrl_GestionTable->pageGestionTable();
          break;

        case 'gestionbdd':
          $this->ctrl_GestionBdd = new ControllerGestionBdd('gestionbdd');
          $this->ctrl_GestionBdd->pageGestionBdd();
          break;

        default:
          $this->ctrl_Admin = new ControllerAdmin('accueil');
          $this->ctrl_Admin->pageAdmin();
          break;


    }//end switch

  }//end function
}//en class
