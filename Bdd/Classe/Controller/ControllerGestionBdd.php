<?php

class ControllerGestionBdd extends Controller {

    public function pageGestionBdd()  {

      if (!empty($_POST)){
            $this->traitementPost();
        }//end if

        $this->vue  = new FabriqueVueAdmin($this->page);
        $this->vue->fabriquePage($this->navTable);
    }//end Function


    private function traitementPost() {

      if (isset($_GET['creation'])) {
        $requete    = '';
        $count      = 0;
        foreach ($_POST as $key => $value) {
          if ($count %2 == 0 && $count != 0) {
            $requete = $requete . ',';
          }
          if (!strstr($key, 'nomtable') && !strstr($key, 'valider')) {
            $requete = $requete . ' ' . $value;
            $count++;
          }

        }//end foreach
        $requete = substr($requete, 0, -1);
        $table = $_POST['nomtable'];
        $this->mod_Admin->creationTable($table, $requete);

      }//end isset

    }//end function

}//end Class

?>
