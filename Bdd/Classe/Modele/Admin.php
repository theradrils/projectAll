<?php

class Admin extends Modele  {

    //prend le nom de l'user dans la table admin au moment de la connexion
    public function getUser($pseudo)  {

        $sql = "SELECT admin.nom as prenom, admin.password as mdp
    FROM admin
    WHERE admin.nom = :pseudo
    ";

        $reponse = $this->execRequete($sql, array(
      'pseudo' => $pseudo
    ));

        return $reponse->fetch(PDO::FETCH_ASSOC);
    }//end function


    public function getAllTable() {

        $sql = "  SELECT table_name FROM information_schema.tables WHERE table_type = 'BASE TABLE' AND table_schema = 'basket' ";
        $reponse = $this->execRequete($sql);
        return $reponse->fetchAll(PDO::FETCH_ASSOC);

    }//end function


    public function getTable($table)  {

        $sql = "SELECT * FROM $table";
        $reponse = $this->execRequete($sql);
        return $reponse->fetchAll(PDO::FETCH_ASSOC);

    }//end function


    public function supprLigne($table, $id) {

        $sql = "DELETE FROM $table WHERE $id[0] = :id";
        $reponse = $this->execRequete(
          $sql,
          array("id" => $id[1]
        ));

    }//end function


  public function modifierTable($tbl, $param) {

    $id_column  = $param->c0->index;
    $id         = $param->c0->contenu;
    $colonne    = '';

    foreach ($param as $key => $val) {
      if ($key != "c0") {
        $colonne .= $val->index . " = '" . $val->contenu . "', ";
      }
    }

    $colonne    = substr($colonne, 0, -2);
    $sql        = "UPDATE $tbl
                    SET   $colonne
                    WHERE $id_column = $id";

    $reponse = $this->execRequete($sql);
  }//end function


  public function ajoutTable($tbl, $param) {

    $colonne  = '';
    $valeur   = '';

    foreach ($param as $key => $val) {
        $colonne .= $val->index . ', ';
        $valeur .= "'" . $val->contenu . "', ";
    }

    $colonne  = substr($colonne, 0, -2);
    $valeur   = substr($valeur, 0, -2);

    $sql = "INSERT INTO $tbl  ($colonne)
       VALUES ($valeur)
       ";

    $reponse = $this->execRequete($sql);
  }//end function


  public function creationTable($table, $donnee) {

    $sql  = "CREATE TABLE $table
              ( romain INT )";
    echo $donnee;
    $reponse = $this->execRequete($sql);

  }//end function



}//end class
