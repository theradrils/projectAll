<?php
abstract class Modele
{
    private $bdd;

    protected function execRequete($sql, $param = null)
    {
        if ($param == null) {
            $resultat = $this->getBdd()->query($sql);
        } else {
            $resultat = $this->getBdd()->prepare($sql);
            $resultat->execute($param);
        }
        return $resultat;
    }

    private function getBdd()
    {
        if ($this->bdd == null) {
            $this->bdd = new PDO('mysql:host=localhost;dbname=basket;charset=utf8', 'romain', 'theradrils', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        }

        return $this->bdd;
    }
}
