PROG = {

  cible       : {},
  tbl_idx_val : {},
  tableCible  : '',
  count       : 0,

  init: function() {

    var url = window.location;

    PROG.affichageMenu();
    PROG.menuChoisi();
    PROG.ecouterSupprAjout();

    //menu surbrillance
    if (!url.search.includes("gestionbdd") && url.search != '') {
      PROG.idNonEditable();
      var btn = document.querySelectorAll('button[name=modifier]');
      var ajout = document.querySelector('button[name=ajouter]');
      ajout.addEventListener('click', PROG.ajout);
      btn.forEach(index => index.addEventListener('click', PROG.action));

    }
  },//end function

  // ecoute les boutton ajout et suppr de ligne pour cree bdd
  ecouterSupprAjout: function () {
    var btn = document.querySelectorAll('.modifTable :nth-child(n+2)');
    for (var i = 0; i < btn.length; i++) {
      btn[i].addEventListener('click', PROG.ajoutOrSuppr);
    }
  },//end function

  // ajoute ou supprime une colonne pour la creation bdd
  ajoutOrSuppr: function (e) {

    btn = e.target.className;

    switch (btn) {

      case 'modifRed':
        var aSuppr = document.querySelector('fieldset').children;
        var supprLigne = document.querySelector('fieldset').removeChild(aSuppr[aSuppr.length - 1]);
        break;

      case 'modifGreen':
        var aAjouter = document.querySelector('fieldset').children;
        aAjouterCopie = aAjouter[3].cloneNode(true);
        aAjouterCopie.children[0].for += PROG.count;
        aAjouterCopie.children[1].name += PROG.count;
        aAjouterCopie.children[2].for += PROG.count;
        aAjouterCopie.children[3].name += PROG.count;
        document.querySelector('fieldset').appendChild(aAjouterCopie);
        break;
      default:

    }
    PROG.count++;
  },//end function

  //fait surbriller les menu choisi
  menuChoisi: function() {

    var url = window.location.search;
    var tbl = [];
    var nav = document.querySelectorAll('nav ul li a');


    for (i = 0; i < nav.length; i++) {
      tbl[i] = nav[i].className;
    }//en for

    if (url == '') {

      nav = document.querySelector('nav ul:first-child li a');
      nav.className = 'selected';
    } else {

      url = url.split('=');

      var premierMenu = url[url.length - 1];
      PROG.tableCible = premierMenu;
      url = url[1].split('&');
      var secondeMenu = url[0];
      if (tbl.includes(premierMenu)) {
        if (premierMenu != secondeMenu) {
          document.querySelector("." + premierMenu).className = 'selected';
        }//end if
        if (premierMenu == secondeMenu) {
          test = document.querySelectorAll('ul')[2].className != 'salut' ? document.querySelectorAll('ul')[2].children[0] : document.querySelectorAll('ul')[1].children[0];
          test.children[0].className += ' selected';
        }//end if
        document.querySelector("." + secondeMenu).className = 'selected';
      }//end if

    }//end if
  },//end function

  //affihe le menu seondaire ou le cache
  affichageMenu: function() {

    var url   = window.location;
    var nav3  = document.querySelector('nav ul:nth-child(3)');
    var nav2  = document.querySelector('nav ul:nth-child(2)');

    if (!url.search.includes('=gestiontable')) {
      nav3.className = 'salut';
    }//end if

    if (!url.search.includes('=gestionbdd')) {
      nav2.className = 'salut';
    }//end if

  },//end function

// modifie une ligne la table choisi
  action: function(e) {

    var colonne   = document.querySelector('.droite').children;
    var divDroite = e.target.parentNode.parentNode.children[1].children;

    for (i = 0; i < divDroite.length; i++) {

      PROG.tbl_idx_val["c".concat(i)] = {
        'index': colonne[i].innerHTML,
        'contenu': divDroite[i].innerHTML
      };
    }//end fot
    PROG.envoi();

  },//end function

//empeche quand ajout de ligne de pouvoir editer l'id
  idNonEditable: function() {

    var id = document.querySelectorAll('.contenu_table .droite');
    id.forEach(index => index.children[0].setAttribute('contenteditable', 'false'));
  },//end function

//copie puis ajoute une ligne dans le DOM puis l'ajoute dans la bdd
  ajout: function() {

    var bordure   = document.querySelector('.bordure');
    bordure       = bordure.cloneNode(true);
    var patron    = document.querySelectorAll('.contenu_table')[0];
    var essai     = patron.cloneNode(true);
    test          = essai.querySelector('.droite');
    gauche        = essai.querySelector('.gauche');

    gauche.removeChild(gauche.children[1]);
    btn = gauche.children[0].children[0].cloneNode(true);
    gauche.removeChild(gauche.children[0]);
    gauche.appendChild(btn);
    gauche.children[0].setAttribute('name', 'ajouter');
    gauche.children[0].innerHTML = "Valider";

    for (var i = 0; i < test.children.length; i++) {
      test.children[i].innerHTML = 'Text a editer';
    }//end for

    test.children[0].innerHTML = 'Non editable';
    test.children[0].setAttribute('contenteditable', 'false');

    document.querySelector('.contenu_all').appendChild(bordure);
    document.querySelector('.contenu_all').appendChild(essai);

    gauche.children[0].addEventListener('click', PROG.enregistrerAjout);
  },//end function

  //Permet de prendre et d'envoyer la ligne seletionner
  enregistrerAjout: function(e) {

    var colonne   = document.querySelector('.droite').children;
    var divDroite = e.target.parentNode.parentNode.children[1].children;

    for (i = 0; i < divDroite.length - 1; i++) {
      PROG.cible["c".concat(i)] = {
        'index': colonne[i + 1].innerHTML,
        'contenu': divDroite[i + 1].innerHTML
      };
    }//end for

    PROG.enregistrer();
  },//end function


  // Envoie des données a php si le champ est remplie
  envoi: function() {

    var xhttp = new XMLHttpRequest();
    xhttp.onload = function() {

      var retour = xhttp.responseText;
    };

    xhttp.open("POST", "index.php?action=gestiontable&table=" + PROG.tableCible, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    contenue = JSON.stringify(PROG.tbl_idx_val);
    var envoi = 'valeur='.concat(contenue);
    xhttp.send(envoi);

  },//end function

  enregistrer: function() {

    var xhttp = new XMLHttpRequest();
    xhttp.onload = function() {

      var retour = xhttp.responseText;
    };

    xhttp.open("POST", "index.php?action=gestiontable&table=" + PROG.tableCible, true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    contenue = JSON.stringify(PROG.cible);
    var envoi = 'ajout='.concat(contenue);
    xhttp.send(envoi);
  }
};//end function
window.onload = PROG.init;
