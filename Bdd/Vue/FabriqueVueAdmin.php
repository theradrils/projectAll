<?php
class FabriqueVueAdmin
{
    private $fichier;

    public function __construct($action = null)
    {
        $this->fichier = ROOT.'/Vue//' . $action . '.php';
    }

    public function fabriquePage($allTable, $donnees = [])
    {
        $contenu = $this->genererPage($this->fichier, $donnees);

        $vue = $this->genererPage(ROOT.'/Vue/templates/gabaritAdmin.php', array('contenu' => $contenu, 'allTable' => $allTable ));
        echo $vue;
    }

    private function genererPage($fichier, $donnees)
    {
        if (file_exists($this->fichier)) {
            extract($donnees);
            ob_start();
            require $fichier;
            return ob_get_clean();
        }
    }
}
