<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Association Basket</title>
    <link rel="stylesheet" href="css/style.css">
    <script src="js/adminModif.js" charset="utf-8"></script>
  </head>
  <body>

    <nav id="nom_table">
        <ul>
            <li > <a class="accueil" href="index.php">Accueil</a> </li>
            <li > <a class="gestionbdd" href="index.php?action=gestionbdd">GestionBdd</a> </li>
            <li > <a class="gestiontable" href="index.php?action=gestiontable">GestionTable</a> </li>
        </ul>
        <ul>
            <li> <a class="creationtable" href="index.php?action=gestionbdd&table=creationtable">Creation Table</a></li>
            <li> <a class="modifiertable" href="index.php?action=gestionbdd&table=modifiertable">Modifier Table</a> </li>
        </ul>
        <ul>
            <?php foreach ($allTable as $key): ?>
                <li><a class="<?php echo $key['table_name'] ?>" href="index.php?action=gestiontable&table=<?php echo $key['table_name'] ?>"><?php echo ucfirst($key['table_name']);  ?></a></li>
            <?php endforeach; ?>
        </ul>
    </nav>

    <div class="main">
        <?= $contenu; ?>
    </div>

    <footer></footer>

  </body>
</html>
